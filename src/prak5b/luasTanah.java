package prak5b;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;
public class luasTanah extends JFrame  {
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 250;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 200;
    private static final int BUTTON_HEIGHT = 25;
    private final JTextField txt, txt1, txt2;

    public static void main(String[] args) {
        luasTanah frame = new luasTanah();
        frame.setVisible(true);
    }

    public luasTanah() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setLayout(null);
        setTitle("Luas Tanah");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        txt = new JTextField();
        txt.setBounds(150, 20, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(txt);

        txt1 = new JTextField();
        txt1.setBounds(150, 60, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(txt1);

        txt2 = new JTextField();
        txt2.setBounds(150, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(txt2);

        JLabel t = new JLabel("Panjang (m)");
        t.setBounds(10, 20, 100, 25);
        contentPane.add(t);

        JLabel t1 = new JLabel("Lebar (m)");
        t1.setBounds(10, 60, 100, 25);
        contentPane.add(t1);

        JLabel t2 = new JLabel("Luas (m2)");
        t2.setBounds(10, 100, 100, 25);
        contentPane.add(t2);

        JButton b = new JButton("Hitung");
        b.setBounds(200, 150, 100, 30);
        contentPane.add(b);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        b.addActionListener(new ActionListener() {
            int angka1, angka2, hasil;

            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    angka1 = Integer.parseInt(txt.getText());
                    angka2 = Integer.parseInt(txt1.getText());
                    hasil = angka1 * angka2;
                    txt2.setText(Integer.toString(hasil));
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Maaf, hanya Integer yang di perbolehkan!",
                            "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        );
        setVisible(true);
        setLocationRelativeTo(null);
    }
}
